﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class inventoryUI : MonoBehaviour
{

    public GameObject button_2;
    public GameObject button_1;
    public GameObject button_3;

    public GameObject ibutton_2;
    public GameObject ibutton_1;
    public GameObject ibutton_3;

    public GameObject Item_logo;
    public GameObject Weapon_logo;
    public GameObject hotbar;
    Swipe swipe;
    float yPos_2;
    float yPos_1;
    float yPos_3;
    float oPos_1;
    float oPos_2;
    float oPos_3;
    float iyPos_2;
    float iyPos_1;
    float iyPos_3;
    float xPos_2;
    float ixPos2;
    bool b_update = false;
    Vector3 maxScale;
    Vector3 minScale;
    public bool weapon = true;
    public bool Swwipe = false;
    Vector2 startTouch;
    // Use this for initialization
    void Awake()
    {
        swipe = GetComponent<Swipe>();
        swipe.SetDeadZone(1000);
        oPos_2 = yPos_2 = button_2.transform.position.y;
        oPos_3 = yPos_3 = button_3.transform.position.y;
        oPos_1 = yPos_1 = button_1.transform.position.y;
        xPos_2 = button_2.transform.position.x;

        iyPos_2 = button_2.transform.position.y;
        iyPos_3 = button_3.transform.position.y;
        iyPos_1 = button_1.transform.position.y;
        ixPos2 = ibutton_2.transform.position.x;


        maxScale = button_2.transform.localScale;
        minScale = button_1.transform.localScale;
    }

    // Update is called once per frame
    public Vector3 StartPos { set { startTouch = value; } }
    void Update()
    {
        Vector2 displacement = Input.touches[0].position - startTouch;
        float tempi = Mathf.Abs((oPos_2 - oPos_1) * 0.5f);
        if (Mathf.Abs(displacement.y) >= tempi)
        {
            if (weapon)
            {
                    if (displacement.y < 0)
                    {
                        if (yPos_2 > oPos_2 - 0.01 && yPos_2 < oPos_2 + 0.01)
                        {
                            button_2.transform.position = new Vector3(button_2.transform.position.x, oPos_3, 0);
                            button_2.transform.localScale = minScale;
                        }
                        else if (yPos_2 > oPos_3 - 0.01 && yPos_2 < oPos_3 + 0.01)
                        {
                            button_2.transform.position = new Vector3(button_2.transform.position.x, oPos_1, 0);
                            button_2.transform.localScale = minScale;
                        }
                        else if (yPos_2 > oPos_1 - 0.01 && yPos_2 < oPos_1 + 0.01)
                        {
                            button_2.transform.position = new Vector3(button_2.transform.position.x, oPos_2, 0);
                            button_2.transform.localScale = maxScale;
                        }
                        yPos_2 = button_2.transform.position.y;

                        if (yPos_1 > oPos_2 - 0.01 && yPos_1 < oPos_2 + 0.01)
                        {
                            button_1.transform.position = new Vector3(button_1.transform.position.x, oPos_3, 0);
                            button_1.transform.localScale = minScale;
                        }
                        else if (yPos_1 > oPos_3 - 0.01 && yPos_1 < oPos_3 + 0.01)
                        {
                            button_1.transform.position = new Vector3(button_1.transform.position.x, oPos_1, 0);
                            button_1.transform.localScale = minScale;
                        }
                        else if (yPos_1 > oPos_1 - 0.01 && yPos_1 < oPos_1 + 0.01)
                        {
                            button_1.transform.position = new Vector3(button_1.transform.position.x, oPos_2, 0);
                            button_1.transform.localScale = maxScale;
                        }
                        yPos_1 = button_1.transform.position.y;

                        if (yPos_3 > oPos_2 - 0.01 && yPos_3 < oPos_2 + 0.01)
                        {
                            button_3.transform.position = new Vector3(button_3.transform.position.x, oPos_3, 0);
                            button_3.transform.localScale = minScale;
                        }
                        else if (yPos_3 > oPos_3 - 0.01 && yPos_3 < oPos_3 + 0.01)
                        {
                            button_3.transform.position = new Vector3(button_3.transform.position.x, oPos_1, 0);
                            button_3.transform.localScale = minScale;
                        }
                        else if (yPos_3 > oPos_1 - 0.01 && yPos_3 < oPos_1 + 0.01)
                        {
                            button_3.transform.position = new Vector3(button_3.transform.position.x, oPos_2, 0);
                            button_3.transform.localScale = maxScale;
                        }
                        yPos_3 = button_3.transform.position.y;
                    }
                    else
                    {
                        if (yPos_2 > oPos_2 - 0.01 && yPos_2 < oPos_2 + 0.01)
                        {
                            button_2.transform.position = new Vector3(button_2.transform.position.x, oPos_1, 0);
                            button_2.transform.localScale = minScale;
                        }
                        else if (yPos_2 > oPos_3 - 0.01 && yPos_2 < oPos_3 + 0.01)
                        {
                            button_2.transform.position = new Vector3(button_2.transform.position.x, oPos_2, 0);
                            button_2.transform.localScale = maxScale;
                        }
                        else if (yPos_2 > oPos_1 - 0.01 && yPos_2 < oPos_1 + 0.01)
                        {
                            button_2.transform.position = new Vector3(button_2.transform.position.x, oPos_3, 0);
                            button_2.transform.localScale = minScale;
                        }
                        yPos_2 = button_2.transform.position.y;

                        if (yPos_1 > oPos_2 - 0.01 && yPos_1 < oPos_2 + 0.01)
                        {
                            button_1.transform.position = new Vector3(button_1.transform.position.x, oPos_1, 0);
                            button_1.transform.localScale = minScale;
                        }
                        else if (yPos_1 > oPos_3 - 0.01 && yPos_1 < oPos_3 + 0.01)
                        {
                            button_1.transform.position = new Vector3(button_1.transform.position.x, oPos_2, 0);
                            button_1.transform.localScale = maxScale;
                        }
                        else if (yPos_1 > oPos_1 - 0.01 && yPos_1 < oPos_1 + 0.01)
                        {
                            button_1.transform.position = new Vector3(button_1.transform.position.x, oPos_3, 0);
                            button_1.transform.localScale = minScale;
                        }
                        yPos_1 = button_1.transform.position.y;

                        if (yPos_3 > oPos_2 - 0.01 && yPos_3 < oPos_2 + 0.01)
                        {
                            button_3.transform.position = new Vector3(button_3.transform.position.x, oPos_1, 0);
                            button_3.transform.localScale = minScale;
                        }
                        else if (yPos_3 > oPos_3 - 0.01 && yPos_3 < oPos_3 + 0.01)
                        {
                            button_3.transform.position = new Vector3(button_3.transform.position.x, oPos_2, 0);
                            button_3.transform.localScale = maxScale;
                        }
                        else if (yPos_3 > oPos_1 - 0.01 && yPos_3 < oPos_1 + 0.01)
                        {
                            button_3.transform.position = new Vector3(button_3.transform.position.x, oPos_3, 0);
                            button_3.transform.localScale = minScale;
                        }
                        yPos_3 = button_3.transform.position.y;
                    }
                    b_update = true;
                startTouch = Input.touches[0].position;
            }
            else
            {
                    if (displacement.y < 0)
                    {
                        if (iyPos_2 > oPos_2 - 0.01 && iyPos_2 < oPos_2 + 0.01)
                        {
                            ibutton_2.transform.position = new Vector3(ibutton_2.transform.position.x, oPos_3, 0);
                            ibutton_2.transform.localScale = minScale;
                        }
                        else if (iyPos_2 > oPos_3 - 0.01 && iyPos_2 < oPos_3 + 0.01)
                        {
                            ibutton_2.transform.position = new Vector3(ibutton_2.transform.position.x, oPos_1, 0);
                            ibutton_2.transform.localScale = minScale;
                        }
                        else if (iyPos_2 > oPos_1 - 0.01 && iyPos_2 < oPos_1 + 0.01)
                        {
                            ibutton_2.transform.position = new Vector3(ibutton_2.transform.position.x, oPos_2, 0);
                            ibutton_2.transform.localScale = maxScale;
                        }
                        iyPos_2 = ibutton_2.transform.position.y;

                        if (iyPos_1 > oPos_2 - 0.01 && iyPos_1 < oPos_2 + 0.01)
                        {
                            ibutton_1.transform.position = new Vector3(ibutton_1.transform.position.x, oPos_3, 0);
                            ibutton_1.transform.localScale = minScale;
                        }
                        else if (iyPos_1 > oPos_3 - 0.01 && iyPos_1 < oPos_3 + 0.01)
                        {
                            ibutton_1.transform.position = new Vector3(ibutton_1.transform.position.x, oPos_1, 0);
                            ibutton_1.transform.localScale = minScale;
                        }
                        else if (iyPos_1 > oPos_1 - 0.01 && iyPos_1 < oPos_1 + 0.01)
                        {
                            ibutton_1.transform.position = new Vector3(ibutton_1.transform.position.x, oPos_2, 0);
                            ibutton_1.transform.localScale = maxScale;

                        }
                        iyPos_1 = ibutton_1.transform.position.y;

                        if (iyPos_3 > oPos_2 - 0.01 && iyPos_3 < oPos_2 + 0.01)
                        {
                            ibutton_3.transform.position = new Vector3(ibutton_3.transform.position.x, oPos_3, 0);
                            ibutton_3.transform.localScale = minScale;

                        }
                        else if (iyPos_3 > oPos_3 - 0.01 && iyPos_3 < oPos_3 + 0.01)
                        {
                            ibutton_3.transform.position = new Vector3(ibutton_3.transform.position.x, oPos_1, 0);
                            ibutton_3.transform.localScale = minScale;
                        }
                        else if (iyPos_3 > oPos_1 - 0.01 && iyPos_3 < oPos_1 + 0.01)
                        {
                            ibutton_3.transform.position = new Vector3(ibutton_3.transform.position.x, oPos_2, 0);
                            ibutton_3.transform.localScale = maxScale;
                        }
                        iyPos_3 = ibutton_3.transform.position.y;
                    }
                    else

                    {
                        if (iyPos_2 > oPos_2 - 0.01 && iyPos_2 < oPos_2 + 0.01)
                        {
                            ibutton_2.transform.position = new Vector3(ibutton_2.transform.position.x, oPos_1, 0);
                            ibutton_2.transform.localScale = minScale;
                        }
                        else if (iyPos_2 > oPos_3 - 0.01 && iyPos_2 < oPos_3 + 0.01)
                        {
                            ibutton_2.transform.position = new Vector3(ibutton_2.transform.position.x, oPos_2, 0);
                            ibutton_2.transform.localScale = maxScale;
                        }
                        else if (iyPos_2 > oPos_1 - 0.01 && iyPos_2 < oPos_1 + 0.01)
                        {
                            ibutton_2.transform.position = new Vector3(ibutton_2.transform.position.x, oPos_3, 0);
                            ibutton_2.transform.localScale = minScale;
                        }
                        iyPos_2 = ibutton_2.transform.position.y;

                        if (iyPos_1 > oPos_2 - 0.01 && iyPos_1 < oPos_2 + 0.01)
                        {
                            ibutton_1.transform.position = new Vector3(ibutton_1.transform.position.x, oPos_1, 0);
                            ibutton_1.transform.localScale = minScale;
                        }
                        else if (iyPos_1 > oPos_3 - 0.01 && iyPos_1 < oPos_3 + 0.01)
                        {
                            ibutton_1.transform.position = new Vector3(ibutton_1.transform.position.x, oPos_2, 0);
                            ibutton_1.transform.localScale = maxScale;
                        }
                        else if (iyPos_1 > oPos_1 - 0.01 && iyPos_1 < oPos_1 + 0.01)
                        {
                            ibutton_1.transform.position = new Vector3(ibutton_1.transform.position.x, oPos_3, 0);
                            ibutton_1.transform.localScale = minScale;

                        }
                        iyPos_1 = ibutton_1.transform.position.y;

                        if (iyPos_3 > oPos_2 - 0.01 && iyPos_3 < oPos_2 + 0.01)
                        {
                            ibutton_3.transform.position = new Vector3(ibutton_3.transform.position.x, oPos_1, 0);
                            ibutton_3.transform.localScale = minScale;

                        }
                        else if (iyPos_3 > oPos_3 - 0.01 && iyPos_3 < oPos_3 + 0.01)
                        {
                            ibutton_3.transform.position = new Vector3(ibutton_3.transform.position.x, oPos_2, 0);
                            ibutton_3.transform.localScale = maxScale;
                        }
                        else if (iyPos_3 > oPos_1 - 0.01 && iyPos_3 < oPos_1 + 0.01)
                        {
                            ibutton_3.transform.position = new Vector3(ibutton_3.transform.position.x, oPos_3, 0);
                            ibutton_3.transform.localScale = minScale;
                        }
                        iyPos_3 = ibutton_3.transform.position.y;
                    }
                    b_update = true;
                startTouch = Input.touches[0].position;
            }
        }

        if(weapon)
        {
            if (iyPos_2 > oPos_2 - 0.01 && iyPos_2 < oPos_2 + 0.01)
                ibutton_2.transform.localScale = minScale;
            else if(iyPos_1 > oPos_2 - 0.01 && iyPos_1 < oPos_2 + 0.01)
                ibutton_1.transform.localScale = minScale;
            else if (iyPos_3 > oPos_2 - 0.01 && iyPos_3 < oPos_2 + 0.01)
                ibutton_3.transform.localScale = minScale;

            if (yPos_2 > oPos_2 - 0.01 && yPos_2 < oPos_2 + 0.01)
                button_2.transform.localScale = maxScale;
            else if (yPos_1 > oPos_2 - 0.01 && yPos_1 < oPos_2 + 0.01)
                button_1.transform.localScale = maxScale;
            else if (yPos_3 > oPos_2 - 0.01 && yPos_3 < oPos_2 + 0.01)
                button_3.transform.localScale = maxScale;

          
            Item_logo.SetActive(false);
            Weapon_logo.SetActive(true);
            hotbar.transform.position = new Vector2(xPos_2, oPos_2);
        }
        else
        {
            if (yPos_2 > oPos_2 - 0.01 && yPos_2 < oPos_2 + 0.01)
                button_2.transform.localScale = minScale;
            else if (yPos_1 > oPos_2 - 0.01 && yPos_1 < oPos_2 + 0.01)
                button_1.transform.localScale = minScale;
            else if (yPos_3 > oPos_2 - 0.01 && yPos_3 < oPos_2 + 0.01)
                button_3.transform.localScale = minScale;

            if (iyPos_2 > oPos_2 - 0.01 && iyPos_2 < oPos_2 + 0.01)
                ibutton_2.transform.localScale = maxScale;
            else if (iyPos_1 > oPos_2 - 0.01 && iyPos_1 < oPos_2 + 0.01)
                ibutton_1.transform.localScale = maxScale;
            else if (iyPos_3 > oPos_2 - 0.01 && iyPos_3 < oPos_2 + 0.01)
                ibutton_3.transform.localScale = maxScale;

            Item_logo.SetActive(true);
            Weapon_logo.SetActive(false);
            hotbar.transform.position = new Vector2(ixPos2, oPos_2);
        }


        if (displacement.y == 0)
        {
            b_update = false;
        }
        if (Input.touchCount > 0)
        {
            if (Input.touches[0].position.x > (xPos_2 + ixPos2) * 0.5)
            {
                weapon = false;
            }
            else
            {
                weapon = true;
            }
        }
        Debug.Log(displacement);
        
    }
}
