﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
public class MyButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{

    public static bool buttonPressed;
    bool buttonRelease;
    public bool inverse = false;
    public GameObject movement;
    public GameObject Inventory;
    public GameObject NPC;
    public GameObject bloodScreen;
    bool attacked = false;
    float timeAttacked = 0;
    float time = 0;
    Vector3 weaponPos;
    Vector3 itemPos;

    public void OnPointerDown(PointerEventData eventData)
    {
        buttonPressed = true;
        buttonRelease = false;
        
    }

    public void OnPointerUp(PointerEventData eventData)
    {

        time = 0;
        buttonPressed = false;
        buttonRelease = true;
    }

    void Start()
    {
        if (inverse)
            buttonPressed = true;
        else
            buttonPressed = false;
        weaponPos = gameObject.transform.position;
        itemPos = weaponPos + new Vector3(50, 0, 0);

    }
    void Update()
    {
        if (!movement)
        {
            movement = Player.Instance.gameObject;
        }

        if (buttonPressed)
            time += Time.deltaTime;

        if (inverse)
        {
            if (Input.touchCount != 0)
            {
                if (Input.touches[0].phase == TouchPhase.Ended || Input.touches[0].phase == TouchPhase.Canceled)
                {
                    movement.GetComponent<movement>().movementFlagUpdate(true);
                    Inventory.gameObject.SetActive(true);
                    gameObject.SetActive(false);
                }
            }
        }
        else
        {
            if(buttonPressed)
            movement.GetComponent<movement>().movementFlagUpdate(false);
            else
                movement.GetComponent<movement>().movementFlagUpdate(true);


            if (time > 0.5f)
            {
                Inventory.gameObject.SetActive(true);
                Inventory.GetComponent<inventoryUI>().StartPos = Input.touches[0].position;
                gameObject.SetActive(false);
                time = 0;
                buttonPressed = false;
                buttonRelease = false;
                Vibration.Vibrate(500);
            }
            else if ((time <= 0.5f && buttonRelease) || (attacked && timeAttacked > 1f))
            {
                if ((movement.gameObject.transform.position - NPC.gameObject.transform.position).magnitude < 2)
                {
                    
                    if (Inventory.GetComponent<inventoryUI>().weapon || attacked)
                    {
                        if (NPC.activeSelf == true)
                        {
                            Vibration.Vibrate(100);
                            timeAttacked = 0;
                            attacked = true;
                            //insert attack here
                            movement.GetComponent<Health>().GetCurrHealth -= 2;
                            if((time <= 0.5f && buttonRelease))
                                NPC.GetComponent<CHealth>().hp -= 10;
                            if (movement.GetComponent<Health>().GetCurrHealth <= 0)
                            {
                                SceneManager.LoadScene("LOSE");
                                Player.Instance.ResetPlayerInfo();
                            }
                            if (NPC.GetComponent<CHealth>().hp <= 0)
                            {
                                NPC.SetActive(false);
                                Player.Instance.GetKills = Player.Instance.GetKills + 1;
                                Player.Instance.GetPlayerLikes = Player.Instance.GetPlayerLikes - 74;
                                Player.Instance.GetPlayerRanking = Player.Instance.GetPlayerRanking - 100;
                                Player.Instance.GetPlayerFollowers = Player.Instance.GetPlayerFollowers - 56;
                                attacked = false;
                                GameObject.Find("NPC_CHAT").SetActive(false);
                            }
                            bloodScreen.SetActive(true);
                            bloodScreen.GetComponent<blood_screen>()._enable = false;
                            buttonRelease = false;
                            
                        }
                    }
                }
            }
            timeAttacked += Time.deltaTime;
        }
    }
}
