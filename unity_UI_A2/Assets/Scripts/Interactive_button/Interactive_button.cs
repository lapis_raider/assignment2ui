﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactive_button : MonoBehaviour {

    static List<GameObject> m_lObjects = new List<GameObject>();
    int m_objectTarget = 0;
    bool buttonEnable = false;

    // Use this for initialization
    void Start () {

	}

    // Update is called once per frame
    void Update()
    {
        if (!buttonEnable)
        {
            for (int i = 0; i < m_lObjects.Count; ++i)
            {
                if (m_lObjects[i].GetComponentInChildren<Proximity_interact>().CheckIfInRange())
                {
                    buttonEnable = true;
                    m_objectTarget = i;
                    break;
                }
            }
        }
        else
        {
            if(!m_lObjects[m_objectTarget].GetComponentInChildren<Proximity_interact>().CheckIfInRange())
            {
                buttonEnable = false;
            }
        }

        if(gameObject.activeSelf != buttonEnable)
        {
            gameObject.SetActive(buttonEnable);
        }
    }

    public void AddList(string name )
    {
        m_lObjects.Add(GameObject.Find(name));
    }
    public void UpdateButton()
    {
    }
}
