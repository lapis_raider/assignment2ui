﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CHealth : MonoBehaviour {

    public int hp = 100;
    public int maxhp = 100;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        int healthPercent = 0;

        if (GetComponent<Health>() != null)
        {
            if (GetComponent<Health>().GetCurrHealth != 0)
                healthPercent = GetComponent<Health>().GetCurrHealth / GetComponent<Health>().GetTotalHealth * 100;
            else
                healthPercent = 0;
        }
        else
        {
            if (hp != 0)
                healthPercent = hp / maxhp * 100;
            else
                healthPercent = 0;
        }
        if (healthPercent == 100)
            GetComponentInChildren<ParticleSystem>().maxParticles = 0;
        else if (healthPercent > 90)
            GetComponentInChildren<ParticleSystem>().maxParticles = 1;
        else if (healthPercent > 80)
            GetComponentInChildren<ParticleSystem>().maxParticles = 2;
        else if (healthPercent > 70)
            GetComponentInChildren<ParticleSystem>().maxParticles = 3;
        else if (healthPercent > 60)
            GetComponentInChildren<ParticleSystem>().maxParticles = 4;
        else if (healthPercent > 50)
            GetComponentInChildren<ParticleSystem>().maxParticles = 5;
        else if (healthPercent > 50)
            GetComponentInChildren<ParticleSystem>().maxParticles = 6;
        else if (healthPercent > 50)
            GetComponentInChildren<ParticleSystem>().maxParticles = 7;
        else if (healthPercent > 50)
            GetComponentInChildren<ParticleSystem>().maxParticles = 8;
        else if (healthPercent > 50)
            GetComponentInChildren<ParticleSystem>().maxParticles = 9;
        else
            GetComponentInChildren<ParticleSystem>().maxParticles = 10;
    }
}
