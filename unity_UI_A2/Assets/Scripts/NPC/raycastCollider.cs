﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class raycastCollider : MonoBehaviour {

    // Use this for initialization
    bool collide = false;
    bool called = false;
    public GameObject g_NPCStats;
    public GameObject g_Player;
    public GameObject g_NPCGlow;
	void Start () {
        collide = false;
        g_NPCStats.gameObject.SetActive(false);
        called = false;
    }
	
	// Update is called once per frame
	void Update () {
        if (!g_Player)
            g_Player = Player.Instance.gameObject;
        Vector2 touchPos = Vector2.zero;
        if (Input.touchCount > 0)
            touchPos = Camera.main.ScreenToWorldPoint(Input.touches[0].position);
        
        RaycastHit2D hit = Physics2D.Raycast(touchPos, Vector2.zero);
        if (hit.collider != null)
        {
            if (hit.collider.name == gameObject.name)
                collide = true;
            else
                collide = false;
        }
        else
            collide = false;

        if (collide && !called)
        {
            Vibration.Vibrate(100);
            g_NPCStats.gameObject.SetActive(!g_NPCStats.activeSelf);
            g_Player.GetComponent<movement>().movementFlagUpdate();
            g_NPCGlow.SetActive(!g_NPCGlow.activeSelf);
            called = true;
        }
        else if (!collide && called)
            called = false;
    }

    bool Collided { get { return collide; } }
}
