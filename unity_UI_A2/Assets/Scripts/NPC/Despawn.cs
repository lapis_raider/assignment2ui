﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Despawn : MonoBehaviour {

    //global vars
    float time = 0;
    public float despawnTime;
    bool startDespawn = false;

    // Use this for initialization
    void Start () {
		
	}

   public void SetDespawn()
    {
        startDespawn = true ;
    }
    void Update()
    {
        if (startDespawn)
            time += Time.deltaTime;
    if (time >= despawnTime)
        {
            time = 0;
            gameObject.SetActive(false);
            startDespawn = false;
        }
    }
}
