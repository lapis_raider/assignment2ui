﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhoneSwipe : MonoBehaviour
{
    // Use this for initialization
    public Swipe swipeData;

    //get the objects that changes will be made to
    private GameObject firstPage;

    private int finalPosX;

    private float timer;

    private float middlePos; //the position we need to translate our pages to
    private float outOfScreenMin;
    private float outOfScreenMax;
    private int currentPage;
    private int prevPage;
    private float nextPos; //next position the one being swipe out have to go to

    public float lerpSpeed;
    public float moveSpeed;
    public float maxSwipeDist;
    public float offsetDist; //the dist to the next point
    public float returnOffset; //check the max dist the entity can go before changing to the other one

    public bool buttonRight;
    public bool buttonLeft;

    public SetAlpha setAlphaUi;

    private void Awake()
    {
        firstPage = GameObject.FindWithTag("FirstScreen");

        middlePos = firstPage.transform.GetChild(0).transform.position.x; //get the middle position
        timer = 0.0f;
        
        swipeData.SetDeadZone(maxSwipeDist);

        RectTransform rt = (RectTransform)(firstPage.transform);
        outOfScreenMin = middlePos - rt.rect.width;
        outOfScreenMax = middlePos + rt.rect.width;

        currentPage = 0;
        prevPage = -1;

        buttonRight = false;
        buttonLeft = false;
    }

    // Update is called once per frame
    void Update()
    {
        timer += lerpSpeed * UnityEngine.Time.deltaTime;

        if (timer > 1.0f)
        {
            timer = 0.0f;
        }

        GameObject other;

        if (swipeData.IsDraggin) //if its dragging
        {
            GameObject current = firstPage.transform.GetChild(currentPage).gameObject;

            float dist = swipeData.SwipeDelta.x * moveSpeed;

            if (swipeData.SwipeDelta.x < 0) //if swiping left
            {
                if (currentPage + 1 < firstPage.transform.childCount) //move the right page too
                {
                    prevPage = currentPage + 1;
                    nextPos = middlePos + offsetDist;
                    other = firstPage.transform.GetChild(currentPage + 1).gameObject;
                    other.transform.position = new Vector3(Mathf.Lerp(other.transform.position.x, other.transform.position.x + dist, timer), current.transform.position.y, 0);
                }

                current.transform.position = new Vector3(Mathf.Lerp(current.transform.position.x, current.transform.position.x + dist, timer), current.transform.position.y, 0);

                if (current.transform.position.x + dist < middlePos - returnOffset) //if its too far, swipe back
                {
                    timer = 0.0f;
                    swipeData.SetDeadZone(true);
                }
            }
            else
            {
                if (currentPage - 1 >= 0) //move the right page too
                {
                    prevPage = currentPage - 1;
                    nextPos = middlePos - offsetDist;
                    other = firstPage.transform.GetChild(currentPage - 1).gameObject;
                    other.transform.position = new Vector3(Mathf.Lerp(other.transform.position.x, other.transform.position.x + dist, timer), current.transform.position.y, 0);
                }

                current.transform.position = new Vector3(Mathf.Lerp(current.transform.position.x, current.transform.position.x + dist, timer), current.transform.position.y, 0);

                if (current.transform.position.x + dist > middlePos + returnOffset)//if its too far, swipe back
                {
                    timer = 0.0f;
                    swipeData.SetDeadZone(true);
                }
            }
        }
        else //if player no longer holding down
        {
            if (swipeData.SwipeRight || buttonRight) //swipe right
            {
                if (currentPage - 1 >= 0)
                {
                    nextPos = middlePos + offsetDist;
                    prevPage = currentPage;
                    currentPage -= 1;
                    timer = 0.0f;
                }

                currentPage = Mathf.Clamp(currentPage, 0, firstPage.transform.childCount); //make sure dont go out of array
                buttonRight = false;
                setAlphaUi.alphaLeft = 1.0f;
                setAlphaUi.alphaRight = 0.5f;
                setAlphaUi.Init();
            }
            else if (swipeData.SwipeLeft || buttonLeft)
            {
                if (currentPage + 1 < firstPage.transform.childCount)
                {
                    nextPos = middlePos - offsetDist;
                    prevPage = currentPage;
                    currentPage += 1;
                    timer = 0.0f;
                }

                currentPage = Mathf.Clamp(currentPage, 0, firstPage.transform.childCount); //make sure dont go out of array
                buttonLeft = false;
                setAlphaUi.alphaLeft = 0.5f;
                setAlphaUi.alphaRight = 1.0f;
                setAlphaUi.Init();
            }
            else //when let go, make the things go into its correct position
            {
                if (prevPage != -1) //if there is a previous page that hasnt move to its right position
                {
                    firstPage.transform.GetChild(prevPage).transform.position = new Vector3(Mathf.Lerp(firstPage.transform.GetChild(prevPage).transform.position.x, nextPos, timer), firstPage.transform.GetChild(currentPage).transform.position.y, 0);
                    if (firstPage.transform.GetChild(prevPage).transform.position.x == nextPos)
                        prevPage = -1;
                }

                firstPage.transform.GetChild(currentPage).transform.position = new Vector3(Mathf.Lerp(firstPage.transform.GetChild(currentPage).transform.position.x, middlePos, timer), firstPage.transform.GetChild(currentPage).transform.position.y, 0);
                Debug.Log("Check " + firstPage.transform.GetChild(currentPage).transform.position.x);
            }
        }


        #region windows Inputs
        if (Input.mousePosition.x < outOfScreenMin || Input.mousePosition.x > outOfScreenMax)
            swipeData.SetDeadZone(true);
        #endregion

        #region MobileInputs
        if (Input.touches.Length > 0) //we holding down something
        {
            if (Input.touches[0].position.x < outOfScreenMin || Input.touches[0].position.x > outOfScreenMax)
                swipeData.SetDeadZone(true);
        }
        #endregion
    }


    public void SetRightButton(bool right)
    {
        buttonRight = right;

    }

    public void SetLeftButton(bool left)
    {
            buttonLeft = left;

    }
}

