﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class InfoStatsPrint : MonoBehaviour {

    public Stats.STATS stat; //choose which stat to print out 
    private TextMeshProUGUI point;

    // Use this for initialization
    void Start () {
        point = gameObject.GetComponent<TextMeshProUGUI>();
    }
	
	// Update is called once per frame
	void Update () {
        point.SetText(Player.Instance.GetPlayerStat.Getstats[(int)stat].ToString()); //access the stat in player to get info
    }
}
