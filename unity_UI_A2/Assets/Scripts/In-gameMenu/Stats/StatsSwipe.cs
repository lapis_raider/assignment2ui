﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatsSwipe : MonoBehaviour {

    // Use this for initialization
    public Swipe swipe;

    public GameObject container; //the gameobject with the list of items we want to swipe with

    public float lerpSpeed;
    public float moveSpeed;
    public float maxSwipeDist;
    public float offsetDist;
    public float returnOffset;
    public int entitiesOnPage; //show the number of eneity to scroll on one page
    public float scaleDiff;

    private bool buttonRight;
    private bool buttonLeft;

    private float middlePos; //the position we need to translate our pages to
    private float outOfScreenMin;
    private float outOfScreenMax;

    private int[] currentEntitiesOnScreen;
    private int currentEntity;
    private int[] outEntity;
    private int noOfEntitiesOnEachSide;
    private float outOfScreenCoords; //teleport the obj to out of screen

    private Vector2 scale; //scale of the entities

    private float nextPos; //next position the one being swipe out have to go to

    private float timer;

    void Start () {
        timer = 0.0f;

        swipe.SetDeadZone(maxSwipeDist);

        middlePos = container.transform.GetChild(0).transform.position.x;
        scale = container.transform.GetChild(0).transform.localScale;

        if (Screen.width == 785)
            offsetDist = 150;

        RectTransform rt = (RectTransform)(container.transform);
        outOfScreenMin = middlePos - rt.rect.width; //the variable to check when its out of screen
        outOfScreenMax = middlePos + rt.rect.width;

        currentEntitiesOnScreen = new int[entitiesOnPage]; //all entities to be shown on screen
        outEntity = new int[container.transform.childCount - entitiesOnPage];


        currentEntity = 0; //current eneity its on
        noOfEntitiesOnEachSide = (int)(entitiesOnPage / 2); //check the number of entities there will be on each side
        //initialise the arrangement
        int checkLeft = 1;
        int checkRight = 1;
        for (int i = 0; i < entitiesOnPage; ++i)
        {
            int next = currentEntity;

            if (checkLeft <= noOfEntitiesOnEachSide)
            {
                next = currentEntity - checkLeft;
                ++checkLeft;
            }
            else if (checkRight <= noOfEntitiesOnEachSide)
            {
                next = currentEntity + checkRight;
                ++checkRight;
            }

            if (next < 0)
                next = container.transform.childCount - 1;
            else if (next >= container.transform.childCount)
                next = 0;

            currentEntitiesOnScreen[i] = next;

            if (container.transform.GetChild(currentEntitiesOnScreen[i]).transform.position.x > outOfScreenMax || container.transform.GetChild(currentEntitiesOnScreen[i]).transform.position.x < outOfScreenMin)
            {
                container.transform.GetChild(currentEntitiesOnScreen[i]).transform.position = new Vector3(outOfScreenMin, container.transform.GetChild(currentEntitiesOnScreen[i]).transform.position.y, 0);
            }
        }
      
        CheckWhichEntityIsOut();

        buttonRight = buttonLeft = false;
        outOfScreenCoords = outOfScreenMin - offsetDist;
    }

    // Update is called once per frame
    void Update()
    { 
        //update the lerp timer
        timer += lerpSpeed * UnityEngine.Time.deltaTime;

        if (timer > 1.0f)
        {
            timer = 0.0f;
        }

        if (swipe.IsDraggin) //if its dragging
        {
            //DraggingMode();
        }
        else
        {
            SwipeMode();
            ReturnToPos();
        }
    }


    void DraggingMode() //player when they dragging instead of swipping
    {
        float dist = swipe.SwipeDelta.x * moveSpeed; //dist the things shopuld be moving

        if (swipe.SwipeDelta.x < 0) //if swiping left
        {
            GameObject current = container.transform.GetChild(currentEntity).gameObject;

            for (int i =0; i < entitiesOnPage; ++i )
            {
                container.transform.GetChild(currentEntitiesOnScreen[i]).transform.position = new Vector3(Mathf.Lerp(container.transform.GetChild(currentEntitiesOnScreen[i]).transform.position.x, container.transform.GetChild(currentEntitiesOnScreen[i]).transform.position.x + dist, timer), container.transform.GetChild(currentEntitiesOnScreen[i]).transform.position.y, 0);

            }
         
            if (current.transform.position.x + dist < middlePos - returnOffset) //when the swipe is not enough, and player let go, return the entity back to its position
            {
                timer = 0.0f;
                swipe.SetDeadZone(true);
            }
            else if (current.transform.position.x + dist > middlePos - returnOffset) //more than the offset, move the currentEntity
            {

            }
        }
        else //if swipping right
        {

        }
    }

    void SwipeMode()
    {
        if (swipe.SwipeRight || buttonRight) //swipe right
        {
            Debug.Log("Swiped right");
            currentEntity -= 1;

            //make the current entity wrap
            if (currentEntity < 0)
                currentEntity = container.transform.childCount - 1;
            else if (currentEntity >= container.transform.childCount)
                currentEntity = 0;


            int checkLeft = 1;
            int checkRight = 1;
            for (int i = 0; i < entitiesOnPage; ++i)
           {
                int next = currentEntity;

                if (checkLeft <= noOfEntitiesOnEachSide)
                {
                    next = currentEntity - checkLeft;
                    ++checkLeft;
                }
                else if (checkRight <= noOfEntitiesOnEachSide)
                {
                    next = currentEntity + checkRight;
                    ++checkRight;
                }

                if (next < 0)
                    next = container.transform.childCount - 1;
                else if (next >= container.transform.childCount)
                    next = 0;

                currentEntitiesOnScreen[i] = next;

                if (container.transform.GetChild(currentEntitiesOnScreen[i]).transform.position.x > outOfScreenMax || container.transform.GetChild(currentEntitiesOnScreen[i]).transform.position.x < outOfScreenMin)
                {
                    container.transform.GetChild(currentEntitiesOnScreen[i]).transform.position = new Vector3(outOfScreenMin, container.transform.GetChild(currentEntitiesOnScreen[i]).transform.position.y, 0);
                }
            }

            outOfScreenCoords = outOfScreenMax + offsetDist;

            CheckWhichEntityIsOut(); //check which entity needs to go out of screen

            timer = 0.0f;
            buttonRight = false;
        }
        else if (swipe.SwipeLeft || buttonLeft)
        {
            Debug.Log("Swiped left");

            currentEntity += 1; //next entity

            //make the current entity wrap
            if (currentEntity < 0)
                currentEntity = container.transform.childCount - 1;
            else if (currentEntity >= container.transform.childCount)
                currentEntity = 0;


            int checkLeft = 1;
            int checkRight = 1;
            for (int i = 0; i < entitiesOnPage; ++i)
            {
                int next = currentEntity;

                if (checkLeft <= noOfEntitiesOnEachSide)
                {
                    next = currentEntity - checkLeft;
                    ++checkLeft; 
                }
                else if (checkRight <= noOfEntitiesOnEachSide)
                {
                    next = currentEntity + checkRight;
                    ++checkRight;
                }

                if (next < 0)
                        next = container.transform.childCount - 1;
                    else if (next >= container.transform.childCount)
                        next = 0;

                currentEntitiesOnScreen[i] = next;

                //check if they are currently out of screen, and teleport them into a position where they will come into screen
                if (container.transform.GetChild(currentEntitiesOnScreen[i]).transform.position.x > outOfScreenMax || container.transform.GetChild(currentEntitiesOnScreen[i]).transform.position.x < outOfScreenMin)
                {
                    container.transform.GetChild(currentEntitiesOnScreen[i]).transform.position = new Vector3(outOfScreenMax, container.transform.GetChild(currentEntitiesOnScreen[i]).transform.position.y, 0);
                }
            }

            outOfScreenCoords = outOfScreenMin - offsetDist; //where the one that are no longer on screen have to go too
            CheckWhichEntityIsOut();

            timer = 0.0f;
            buttonLeft = false;
        }
    }


    void ReturnToPos()
    {
        int right = 0;
        int left = 0;
        for (int i = 0; i < entitiesOnPage; ++i)
        {
            //check if its left
            if ((currentEntitiesOnScreen[i] < currentEntity || (currentEntity - 1 < 0 && currentEntitiesOnScreen[i] > currentEntity)) && left < noOfEntitiesOnEachSide)
            {
                container.transform.GetChild(currentEntitiesOnScreen[i]).transform.position = new Vector3(Mathf.Lerp(container.transform.GetChild(currentEntitiesOnScreen[i]).transform.position.x, middlePos - offsetDist, timer), container.transform.GetChild(currentEntitiesOnScreen[i]).transform.position.y, 0);
                container.transform.GetChild(currentEntitiesOnScreen[i]).transform.localScale = new Vector3(Mathf.Lerp(container.transform.GetChild(currentEntitiesOnScreen[i]).transform.localScale.x, scale.x * scaleDiff, timer), Mathf.Lerp(container.transform.GetChild(currentEntitiesOnScreen[i]).transform.localScale.y, scale.y * scaleDiff, timer), 0);

                ++left;
            }

            //check if its on the right
            else if (right < noOfEntitiesOnEachSide && (currentEntitiesOnScreen[i] > currentEntity || (currentEntity + 1 >= container.transform.childCount && currentEntitiesOnScreen[i] < currentEntity)))
            {
                container.transform.GetChild(currentEntitiesOnScreen[i]).transform.position = new Vector3(Mathf.Lerp(container.transform.GetChild(currentEntitiesOnScreen[i]).transform.position.x, middlePos + offsetDist, timer), container.transform.GetChild(currentEntitiesOnScreen[i]).transform.position.y, 0);
                container.transform.GetChild(currentEntitiesOnScreen[i]).transform.localScale = new Vector3(Mathf.Lerp(container.transform.GetChild(currentEntitiesOnScreen[i]).transform.localScale.x, scale.x * scaleDiff, timer), Mathf.Lerp(container.transform.GetChild(currentEntitiesOnScreen[i]).transform.localScale.y, scale.y * scaleDiff, timer), 0);
                ++right;
            }

            else //in the centre
            {
                container.transform.GetChild(currentEntitiesOnScreen[i]).transform.position = new Vector3(Mathf.Lerp(container.transform.GetChild(currentEntitiesOnScreen[i]).transform.position.x, middlePos, timer), container.transform.GetChild(currentEntitiesOnScreen[i]).transform.position.y, 0);
                container.transform.GetChild(currentEntitiesOnScreen[i]).transform.localScale = new Vector3(Mathf.Lerp(container.transform.GetChild(currentEntitiesOnScreen[i]).transform.localScale.x, scale.x, timer), Mathf.Lerp(container.transform.GetChild(currentEntitiesOnScreen[i]).transform.localScale.y, scale.y, timer), 0);
            }
        }

        //make those out of screen entities go out of screen
        for (int i = 0; i < container.transform.childCount - entitiesOnPage; ++i)
        {
            if (container.transform.GetChild(outEntity[i]).transform.position.x < outOfScreenMax + offsetDist && container.transform.GetChild(outEntity[i]).transform.position.x > outOfScreenMin - offsetDist)
                container.transform.GetChild(outEntity[i]).transform.position = new Vector3(Mathf.Lerp(container.transform.GetChild(outEntity[i]).transform.position.x, outOfScreenCoords, timer), container.transform.GetChild(outEntity[i]).transform.position.y, 0);

            container.transform.GetChild(outEntity[i]).transform.localScale = new Vector3(Mathf.Lerp(container.transform.GetChild(outEntity[i]).transform.localScale.x, scale.x * scaleDiff, timer), Mathf.Lerp(container.transform.GetChild(outEntity[i]).transform.localScale.y, 0.0f, timer), 0);
        }
    }

    void CheckWhichEntityIsOut()
    {
        //reset
        for (int i = 0; i < container.transform.childCount - entitiesOnPage; ++i)
        {
            outEntity[i] = -1; 
        }

        for (int i = 0; i < container.transform.childCount; ++i)
        {
            bool storeThis = true;

            for (int r = 0; r < entitiesOnPage; ++r)
            {
                if (i == currentEntitiesOnScreen[r]) //check if its inside the current enetities on screen, if yes, do not store
                {
                    storeThis = false;
                    break;
                }
            }

            if (storeThis)
            {
                for (int r =0; r < container.transform.childCount - entitiesOnPage; ++r)
                {
                    outEntity[r] = i; //store the ones that are not here
                }
            }
        }
    }

    public int GetCurrent
    {
        get { return currentEntity; }
    }


    public void SetRightButton(bool right)
    {
        buttonRight = right;
    }

    public void SetLeftButton(bool left)
    {
        buttonLeft = left;
    }

}
