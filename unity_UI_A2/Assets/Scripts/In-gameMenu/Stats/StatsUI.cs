﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class StatsUI : MonoBehaviour {

    // Use this for initialization
    public Image statsProgressionBar;
    public TextMeshProUGUI upgradePtsLeft; //text to tell how many upgrade pts left
    public TextMeshProUGUI statDesciption; //describe the stat

    private int current;

    void Start () {
        current = -1;
    }
	
	// Update is called once per frame
	void Update () {
       
        if (current != gameObject.GetComponent<StatsSwipe>().GetCurrent) //check if current change or not, if changed, change the desciption
        {
            current = gameObject.GetComponent<StatsSwipe>().GetCurrent;
            UpdateStatDesciption();
        }

        UpdatePtsLeftText();
        UpdateProgressBar(); //upgrade the progress bar
    }

    void UpdateProgressBar()
    {
        statsProgressionBar.fillAmount = (float)Player.Instance.GetPlayerStat.Getstats[current] / (float)Player.Instance.maxStatPt;
    }

    void UpdatePtsLeftText()
    {
        upgradePtsLeft.SetText( "Upgrade points left:   " + Player.Instance.GetPlayerStat.GetUpdgradePts.ToString());
    }

    void UpdateStatDesciption()
    {
        statDesciption.SetText(Player.Instance.GetPlayerStat.GetstatDescriptions[current]);
    }

    public void UpgradePts()
    {
        if (Player.Instance.GetPlayerStat.GetUpdgradePts > 0) //if player has points to use
        {
            if (Player.Instance.GetPlayerStat.Getstats[current] < Player.Instance.maxStatPt) //if the current stat have not reached the max level yet
            {
                Player.Instance.GetPlayerStat.Getstats[current] += 1;
                Player.Instance.GetPlayerStat.GetUpdgradePts -= 1;
            }
        }
    }
}
