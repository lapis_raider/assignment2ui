﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class SavePopUp : MonoBehaviour {

    // Use this for initialization
    public GameObject yesButton;
    public GameObject noButton;
    public TextMeshProUGUI time;
    public TextMeshProUGUI killsCount;
    public TextMeshProUGUI warningStatement;

    bool overwriteState = false; //check whether player press to save or not
    float timer = 0.0f;

    // Update is called once per frame
    void Update() {

        if (overwriteState)
        {
            #region windows Inputs
            if (Input.GetMouseButton(0)) //if player tap anywhere, just make the popup disappear
                gameObject.SetActive(false);
            #endregion

            #region MobileInputs
            if (Input.touches.Length > 0)
                if (Input.touches[0].phase == TouchPhase.Began)
                     gameObject.SetActive(false);
            #endregion

            //if player never click anything, wait for a certain amt of time before making popup disappear
            timer += Time.deltaTime;

            if (timer > 3.0f)
                gameObject.SetActive(false);
        }

    }

    public void Init()
    {
        //set the originally saved kills and time
        warningStatement.SetText("Overwrite previous file?");
        killsCount.SetText("<b>Kills:</b> " + Saving.Instance.GetKillCount.ToString());
        time.SetText("<b>Time:</b> " + Saving.Instance.GetHourSaved.ToString() + ":" + Saving.Instance.GetMinsSaved.ToString());
        //set the color
        warningStatement.color = new Color32(255, 186, 186, 255);
        killsCount.color = new Color32(255, 186, 186, 255);
        time.color = new Color32(255, 186, 186, 255);

        //make the buttons able to interact
        yesButton.SetActive(true);
        noButton.SetActive(true);
        overwriteState = false;
        gameObject.SetActive(true);
    }


    public void OnYesPress()
    {
        //change the text and update to the current kill and time count
        warningStatement.SetText("File saved");
        killsCount.SetText("<b>Kills:</b> " + Player.Instance.GetKills.ToString());
        time.SetText("<b>Time:</b> " + Player.Instance.GetHour.ToString() + ":" + Player.Instance.GetMins.ToString());


        //change the text color
        warningStatement.color = new Color32(255, 211, 127, 255);
        killsCount.color = new Color32(255, 211, 127, 255);
        time.color = new Color32(255, 211, 127, 255);

        //make the buttons able to interact
        yesButton.SetActive(false);
        noButton.SetActive(false);
        overwriteState = true;

        //write in the things
        Saving.Instance.GetSaved = true;
        Saving.Instance.GetKillCount = Player.Instance.GetKills;
        Saving.Instance.GetMinsSaved = Player.Instance.GetMins;
        Saving.Instance.GetHourSaved = Player.Instance.GetHour;

        timer = 0.0f;
    }

    public void OnNoPress()
    {
        gameObject.SetActive(false);
    }

}
