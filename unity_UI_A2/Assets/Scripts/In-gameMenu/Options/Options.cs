﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class Options : MonoBehaviour {

    // Use this for initialization
    public AudioMixer music;
    public AudioMixer soundFX;

    void Start () {
		
	}
	
	// Update is called once per frame

    public void SetVolumeMusic(float vol)
    {
        music.SetFloat("musicAudio", vol);
    }

    public void SetVolumeSoundEffects(float vol)
    {
        soundFX.SetFloat("soundFX", vol);
    }

    public void Vibrations(bool vir)
    {
        Saving.Instance.GetVibration = vir;
    }
}
