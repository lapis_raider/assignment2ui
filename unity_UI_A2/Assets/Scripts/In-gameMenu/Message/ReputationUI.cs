﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class ReputationUI : MonoBehaviour {

    public TextMeshProUGUI followers;
    public TextMeshProUGUI ranking;
    public TextMeshProUGUI likes;
    public GameObject msg; //the messages
    public Image playerIcon;
    public TextMeshProUGUI playerInfo;

    public void Init()
    {
        followers.SetText(Player.Instance.GetPlayerFollowers.ToString());
        ranking.SetText(Player.Instance.GetPlayerRanking.ToString());
        likes.SetText(Player.Instance.GetPlayerLikes.ToString());

        MessageUI ui = msg.GetComponent<MessageUI>();

        if (Player.Instance.GetPlayerRanking > 150)
        {
            ui.UpdateMessages(MessageUI.MsgState.GREAT);
            playerIcon.sprite = Resources.Load<Sprite>("In-GameUI/Reputation/notCrazyIcon");
            playerInfo.SetText("Just your average school girl looking for love \n <b> Love: </b> Romance <b> \n Interests: </b> Romance <b> \n Live: </b> Seattle");
        }
        else if (Player.Instance.GetPlayerRanking < 50)
        {
            ui.UpdateMessages(MessageUI.MsgState.BAD);
            playerIcon.sprite = Resources.Load<Sprite>("In-GameUI/Reputation/crazyIcon");
            playerInfo.SetText("Just your average school girl looking for love \n <b> Love: </b> Killing <b> \n Interests: </b> YOU <b> \n Live: </b> ???");
        }
        else
        {
            ui.UpdateMessages(MessageUI.MsgState.NEUTRAL);
            playerIcon.sprite = Resources.Load<Sprite>("In-GameUI/Reputation/notCrazyIcon");
            playerInfo.SetText("Just your average school girl looking for love \n <b> Love: </b> Romance <b> \n Interests: </b> Romance <b> \n Live: </b> Seattle");
        }
    }
}
