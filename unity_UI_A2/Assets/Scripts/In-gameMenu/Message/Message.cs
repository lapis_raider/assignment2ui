﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Message : MonoBehaviour {

    // Use this for initialization
    public Image icon;
    public TextMeshProUGUI userName;
    public TextMeshProUGUI text;
}
