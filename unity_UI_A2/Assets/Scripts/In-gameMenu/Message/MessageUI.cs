﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class MessageUI : MonoBehaviour {

    // Use this for initialization
    public enum MsgState
    {
        BAD,
        NEUTRAL,
        GREAT
    }

    Message[] slots;

    string[,] messages =
    {
        { "Did you just kill someone??", "Man, this gurl cray cray.", "GURL, YOU NEED THE POLICE!", "Wtf just happen?? Did I just saw a murder attempt?", "Stop this you psycho! This is why no one likes you!", "STAY AWAY FROM ME", "HELLO 995, WE GOT A PSYCHO HERE", "AHHHHHHHHHHHHHhhhhhh",  ":(" },
        { "REMINDER: hand up your homework", "You're a little weird", "Thanks for helping me the other day!", "Llamas are great animals, so fluffy", "Please I'm so tired, I just want an A grade", "RIP UMISOFT hello FinLend, definitely nothing releated to Ubisoft", "Good try", "Do you like dogs?", "May the ships be canon" },
        { "OMG, BEST GURL", "We need to hang up some times", "OMG, you're so prettyyyy, you need to teach me your fashion skills", "Pls call me, 555-555-555", "Do you want to go out on a date with me?", "Omg you are the best person in the world", "Wow, please teach me how to be popular", "Thank you for helping me out the other day!",  "Want to go to the nearby cafe later?" },
    };

    string[] userNames = { "@SomeHighSchoolGirl", "@Legend27", "@CallMeDaddy.com", "@TheGoodLookinOne", "@GuitaristPro2000", "@GamerBoiLookinForLove", "@Depression101", "@AlpacaLover", "@BestFriendForlife", "@BetterYandere", "@ProgrammingIsCool", "@PlsGiveAGrade", "@ILikeBannanas" };

    string[] photos = { "In-GameUI/Reputation/NPC1Icon", "In-GameUI/Reputation/NPC2Icon", "In-GameUI/Reputation/NPC3Icon", "In-GameUI/Reputation/NPC4Icon" , "In-GameUI/Reputation/NPC5Icon" , "In-GameUI/Reputation/NPC6Icon", "In-GameUI/Reputation/NPC7Icon", "In-GameUI/Reputation/NPC8Icon" };

    void Awake () {
        slots = gameObject.GetComponentsInChildren<Message>();
    }


    public void UpdateMessages(MessageUI.MsgState msg)
    {
        Random.InitState(System.DateTime.Now.Millisecond);
        //change it accordingly
        for (int i=0; i < slots.Length; ++i)
        {
            slots[i].userName.SetText(userNames[Random.Range(0, userNames.Length)]);
            slots[i].text.SetText(messages[(int)msg, Random.Range(0, 8)]);
            slots[i].icon.sprite = Resources.Load<Sprite>(photos[Random.Range(0, photos.Length)]);
        }
    }
}
