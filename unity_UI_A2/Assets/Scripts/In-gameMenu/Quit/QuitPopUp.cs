﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class QuitPopUp : MonoBehaviour {

    // Use this for initialization
    public void OnYesButtonClicked()
    {
        SceneManager.LoadScene("StartMenu");
    }

    public void OnNoButtonClicked()
    {
        gameObject.SetActive(false);
    }
}
