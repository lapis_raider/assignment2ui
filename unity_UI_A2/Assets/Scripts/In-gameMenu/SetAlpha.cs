﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetAlpha : MonoBehaviour {

    // Use this for initialization
    public float alphaRight;
    public float alphaLeft;
    public Button leftButton;
    public Button rightButton;

    public void Start()
    {
        var tempColor = leftButton.image.color;
        tempColor.a = alphaLeft; //1f makes it fully visible, 0f makes it fully transparent.
        leftButton.image.color = tempColor;

        tempColor = rightButton.image.color;
        tempColor.a = alphaRight; //1f makes it fully visible, 0f makes it fully transparent.
        rightButton.image.color = tempColor;
    }

    // Update is called once per frame
    public void Init () {
        var tempColor = leftButton.image.color;
        tempColor.a = alphaLeft; //1f makes it fully visible, 0f makes it fully transparent.
        leftButton.image.color = tempColor;

        tempColor = rightButton.image.color;
        tempColor.a = alphaRight; //1f makes it fully visible, 0f makes it fully transparent.
        rightButton.image.color = tempColor;
    }
}

