﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoveMailUI : MonoBehaviour {

    // Use this for initialization
    public GameObject heartFill;
    public GameObject msgContainer;
    float timer = 0.0f;
    string nameOfLover = "burondo";
    bool afterYou = false;

	void Start () {
        msgContainer.GetComponent<LoveMsgContainer>().Init(nameOfLover);
    }
	
	// Update is called once per frame
	void Update () {

        timer += Time.deltaTime;

        if (!afterYou && Player.Instance.GetKills != 0) //when player first killed girl
        {
           msgContainer.GetComponent<LoveMsgContainer>().AddKill(nameOfLover); //add the killed message
           nameOfLover = "YOU";
           timer = 0.0f;
           afterYou = true;
        }

        if (timer > 2.0f) //after a certain time period, add more love
        {
            msgContainer.GetComponent<LoveMsgContainer>().Add(nameOfLover);

            timer = 0.0f;

            if (nameOfLover == "YOU") //if youre the lover, increase heart
            {
                heartFill.transform.localPosition = new Vector2(heartFill.transform.localPosition.x, heartFill.transform.localPosition.y + 20.0f);
                //about heartFill.transform.localPosition.y > 11 is win
                if (heartFill.transform.localPosition.y > 11)
                {
                    SceneManager.LoadScene("WIN");
                }

            }
        }
    }
}
