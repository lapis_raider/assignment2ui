﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoveMsgContainer : MonoBehaviour {

    // Use this for initialization
    LoveMsg[] slots;

    void Awake () {
        slots = gameObject.GetComponentsInChildren<LoveMsg>();
    }

    public void Init(string name)
    {
        for (int i = 0; i < slots.Length; ++i)
        {
            slots[i].npcName.SetText("FutureBoyfriend-senpai-kun");
            slots[i].message.SetText("Sent hearts to <u>" + name + "</u>");
            slots[i].time.SetText((i + 1).ToString() + ".p.m");
            slots[i].icon.sprite = Resources.Load<Sprite>("In-GameUI/LoveMail/boyfriendIcon");
        }
    }

    public void Add(string name)
    {
        //push back everything
        for (int i = 0; i < slots.Length - 1; ++i)
        {
            slots[i].npcName.SetText(slots[i + 1].npcName.text);
            slots[i].message.SetText(slots[i + 1].message.text);
            slots[i].time.SetText(slots[i + 1].time.text);
            slots[i].icon.sprite = slots[i + 1].icon.sprite;
            Debug.Log(slots[i].time);
        }

        slots[slots.Length - 1].npcName.SetText("FutureBoyfriend-senpai-kun");
        slots[slots.Length - 1].message.SetText("Sent hearts to <u>" + name + "</u>");
        slots[slots.Length - 1].time.SetText(Player.Instance.GetTime.ToString() + ".p.m");
        slots[slots.Length - 1].icon.sprite = Resources.Load<Sprite>("In-GameUI/LoveMail/boyfriendIcon");
    }

    public void AddKill(string name)
    {
        //push back everything
        for (int i = 0; i < slots.Length - 1; ++i)
        {
            slots[i].npcName = slots[i + 1].npcName;
            slots[i].message = slots[i + 1].message;
            slots[i].time = slots[i + 1].time;
            slots[i].icon.sprite = slots[i + 1].icon.sprite;
        }

        slots[slots.Length - 1].npcName.SetText("<s>" + name + "-chan</s>");
        slots[slots.Length - 1].message.SetText("Account was deactivated...");
        slots[slots.Length - 1].time.SetText(Player.Instance.GetTime.ToString() + ".p.m");
        slots[slots.Length - 1].icon.sprite = Resources.Load<Sprite>("In-GameUI/LoveMail/otherGirlIcon");
    }
}
