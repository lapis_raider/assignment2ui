﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class LoveMsg : MonoBehaviour {

    // Use this for initialization
    public TextMeshProUGUI npcName;
    public TextMeshProUGUI message;
    public TextMeshProUGUI time;
    public Image icon;
}
