﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MovingTextures : MonoBehaviour {

    // Use this for initialization
    public float xScrollSpeed;
    public float yScrollSpeed;
    float timer = 0;

    // Update is called once per frame
    void Update () {
        float xOffset = Time.time * xScrollSpeed;
        float yOffset = Time.time * yScrollSpeed;

        GetComponent<Image>().material.mainTextureOffset = new Vector2(xOffset, yOffset); //update the material
	}
}
