﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfoUI : MonoBehaviour {

    // Use this for initialization
    [SerializeField]
    GameObject selectMode; //info is shown
    [SerializeField]
    GameObject NoSelectMode; //character sprite is shown


    public void Activated(ObjectType obj)
    {
        if (obj == null) //if theres no obj inside
        {
            NoSelectMode.SetActive(true); //activate select mode
            selectMode.SetActive(false);
        }
        else
        {
            NoSelectMode.SetActive(false);
            selectMode.SetActive(true);
            selectMode.GetComponent<InventoryInfo>().ChangeInfo(obj);//change the infomation on the info screen
        }
    }
}
