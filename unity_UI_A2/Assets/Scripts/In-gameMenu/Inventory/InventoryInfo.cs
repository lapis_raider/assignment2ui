﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class InventoryInfo : MonoBehaviour {

    // Use this for initialization
    public Image imageSprite;
    public TextMeshProUGUI itemInfo;
    public TextMeshProUGUI itemName;

    public void ChangeInfo(ObjectType obj)
    {
        //change the infomation
        itemName.SetText(obj.objName);
        itemInfo.SetText(obj.objDescription);
        imageSprite.sprite = obj.objSprite;
    }
}
