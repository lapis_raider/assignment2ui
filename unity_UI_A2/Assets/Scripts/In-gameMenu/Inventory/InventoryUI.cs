﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryUI : MonoBehaviour {

    // Use this for initialization
    Inventory inventory;
    InventorySlots[] slots;

    private void Start()
    {
        inventory = Inventory.Instance;
        //inventory.onItemChangedCallback += UpdateUI;
        slots = gameObject.GetComponentsInChildren<InventorySlots>();
    }


    // Update is called once per frame
    void Update () {
        UpdateUI();
    }

    void UpdateUI()
    {
        for (int i=0; i < inventory.weaponSpace; ++i)
        {
            if (i < inventory.weaponsList.Count)
                slots[i].AddItem(inventory.weaponsList[i]);
            else
                slots[i].RemoveItem();
        }

        for (int i = 0; i < inventory.itemNormalSpace; ++i)
        {
            if (i < inventory.itemNormalList.Count)
                slots[i + inventory.weaponSpace].AddItem(inventory.itemNormalList[i]);
            else
                slots[i + inventory.weaponSpace].RemoveItem();
        }
    }
}
