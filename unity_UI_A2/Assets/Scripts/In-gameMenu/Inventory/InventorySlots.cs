﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventorySlots : MonoBehaviour {

    // Use this for initialization
    public Image icon;
    ObjectType item;

    public InfoUI showItemInfo;
    public Button removeButton;
    public GameObject popUp;

    private float holdTimer = 0.0f;

    public void Update()
    {
        #region windows Inputs
        if (Input.GetMouseButton(0))
        {
            holdTimer += Time.deltaTime;
        }
        else
            holdTimer = 0.0f;
        #endregion

        #region MobileInputs
        if (Input.touches.Length > 0)
        {
            if (Input.touches[0].phase == TouchPhase.Began)
                holdTimer += Time.deltaTime;
            else if ((Input.touches[0].phase == TouchPhase.Ended || Input.touches[0].phase == TouchPhase.Canceled))
                holdTimer = 0.0f;
        }
        #endregion


        if (holdTimer > 1.0f && item != null)
        {
            removeButton.interactable = true; //after holding for a certain amount of time, show the cancel option
            holdTimer = 0.0f;
            popUp.GetComponent<InventoryPopUp>().activateDelete = true;
        }

        if (!popUp.GetComponent<InventoryPopUp>().activateDelete) //so all delete button will disappear
        {
            removeButton.interactable = false;
        }
    }

    public void AddItem(ObjectType newItem)
    {
        item = newItem;
        icon.sprite = newItem.objSprite;
        icon.enabled = true;
    }

    public void RemoveItem()
    {
        item = null;
        icon.sprite = null;
        icon.enabled = false;
    }

    public void OnRemoveButton() //when asked to remove item
    {
        popUp.SetActive(true); //set it to active, show the popup
        popUp.GetComponent<InventoryPopUp>().Activated(item);
    }

    public void CheckItemInfo() //use this to activate the item info section
    {
        showItemInfo.Activated(item);
    }
}
