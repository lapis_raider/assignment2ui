﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryPopUp : MonoBehaviour {

    // Use this for initialization
    public Image deleatedItem;
    public InfoUI info;
    bool crossActivated;
    private ObjectType item;

	public void Start () {
        crossActivated = false;
    }

    public void Activated(ObjectType obj)
    {
        item = obj;
        deleatedItem.sprite = obj.objSprite;
    }

    public void OnYesClick()
    {
        if (item != null) //remove the item
        {
            Inventory.Instance.Remove(item);
            info.Activated(null);
        }

        crossActivated = false;
        gameObject.SetActive(false);
    }

    public void OnNoClick()
    {
        crossActivated = false;
        gameObject.SetActive(false);
    }

    public bool activateDelete
    { get { return crossActivated; }
        set { crossActivated = value; }
    }
}
