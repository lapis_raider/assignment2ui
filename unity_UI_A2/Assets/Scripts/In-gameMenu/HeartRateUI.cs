﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeartRateUI : MonoBehaviour {

    // Use this for initialization
    private MovingTextures movingTexture;
    public int bloodSpawnRate; //how much damage or health increase player take, to spawn blood splatter
    public GameObject lifeLineBad;
    public GameObject lifeLineGood;
    public Image heart = null;

    private Vector2 minPos;
    private Vector2 maxPos;
    private int playerLastHealth;



    bool done = true;

    void Awake () {
        movingTexture = GetComponentInChildren<MovingTextures>(); //get the moving texture for the heartrate
                                                                  // lifeLine.material = Resources.Load<Material>("Assets/Materials/heatrRate.mat");
                                                                  // lifeLine.material = Resources.Load("In-GameUI/HeartMonitor/Materials/FastHeartRate") as Material;

        //movingTexture.xScrollSpeed = 0.5f;
        RectTransform rt = (RectTransform)(gameObject.transform);
        Vector2 middlePos = new Vector2(transform.position.x, transform.position.y);
        
        minPos = new Vector2(middlePos.x - rt.rect.width, middlePos.y - rt.rect.height);
        maxPos = new Vector2(middlePos.x + rt.rect.width, middlePos.y + rt.rect.height);

        playerLastHealth = Player.Instance.GetPlayerHealth.GetTotalHealth;
    }
	
	// Update is called once per frame
	void Update () {
        //create the bloodsplatters
        if (done && Player.Instance.GetPlayerHealth.GetCurrHealth < Player.Instance.GetPlayerHealth.GetTotalHealth / 2)
        {
            lifeLineGood.SetActive(false);
            lifeLineBad.SetActive(true);
            heart.sprite = Resources.Load<Sprite>("In-GameUI/HeartMonitor/Images/heartDying");
            done = false;
        }
    }

    public void Init()
    {
        int healthDiff = playerLastHealth - Player.Instance.GetPlayerHealth.GetCurrHealth;

        int numberToSpawn = healthDiff / bloodSpawnRate;

        if (numberToSpawn < gameObject.transform.Find("heartparticle").childCount)
        {
            int deleteAmt = gameObject.transform.Find("heartparticle").childCount - numberToSpawn;

            for (int i = 0; i < deleteAmt; ++i)
            {
                DestroyBloodSplatter();
            }

            playerLastHealth = Player.Instance.GetPlayerHealth.GetCurrHealth;
        }

        if (numberToSpawn > gameObject.transform.Find("heartparticle").childCount)
        {
            int increaseAmt = numberToSpawn - gameObject.transform.Find("heartparticle").childCount;

            for (int i = 0; i < increaseAmt; ++i)
            {
                CreateBloodSplatters();
            }

            playerLastHealth = Player.Instance.GetPlayerHealth.GetCurrHealth;
        }
    }

    void CreateBloodSplatters()
    {
        GameObject img = null;

        if (Random.Range(0, 2) == 1)
             img = Resources.Load<GameObject>("In-GameUI/HeartMonitor/Prefabs/BloodSplatterParticle");
        else
            img = Resources.Load<GameObject>("In-GameUI/HeartMonitor/Prefabs/BloodSplatterParticle2");

        img.transform.localScale = new Vector2(Random.Range(1.2f, 4.5f), Random.Range(1.2f, 4.5f)); //randomize its size
        var bloodSplatter = Instantiate(img, new Vector2(Random.Range(minPos.x, maxPos.x), Random.Range(minPos.y, maxPos.y)), transform.rotation ); //randomize its rotation and position

        bloodSplatter.transform.SetParent(gameObject.transform.Find("heartparticle"));
    }

    void DestroyBloodSplatter()
    {
        if (gameObject.transform.Find("heartparticle").childCount <= 0)
            return;

        Destroy(gameObject.transform.Find("heartparticle").GetChild(0).gameObject);
    }
}
