﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class blood_screen : MonoBehaviour {

    // Use this for initialization
    bool start = false;
    float time = 0;
    public bool _enable
    {
        get { return start; }
        set { this.start = value; }
    }
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        time += Time.deltaTime;
        if (!start)
        {
          GetComponent<Animator>().Play(0, 0, 0.50f);
            start = true;
            Debug.Log("Blood Screen animation Enabled");
        }
       if(time >= 0.05f)
        {
            gameObject.SetActive(false);
            time = 0;
        }
    }
}
