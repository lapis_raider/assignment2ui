﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Proximity_interact : MonoBehaviour {

    public GameObject m_interactiveButton;
    public float m_range;
	// Use this for initialization
	void Start () {
        //m_interactiveButton.GetComponent<Interactive_button>().AddList(this.name);
     
    }
    public Object_enum.ObjectType GetObjectType()
    {
        return transform.parent.gameObject.transform.parent.gameObject.GetComponent<ObjectType>().GetObjectType();
    }
    public bool CheckIfInRange()
    {
        return false;
    }

    public void ButtonUpdate()
    {
        gameObject.transform.parent.gameObject.SetActive(false);
        Inventory.Instance.AddItem(gameObject.transform.parent.gameObject.transform.parent.gameObject.GetComponent<ObjectType>()); //ADD THE OBJECT INTO THE INVENTORY
    }

    // Update is called once per frame
    void Update () {

        if (GetComponent<proximity_glow>() != null)
        {
            if (GetComponent<proximity_glow>().CheckIsInRange(m_range))
            {
                m_interactiveButton.SetActive(true);
            }
            else
                m_interactiveButton.SetActive(false);
        }
    }

}
