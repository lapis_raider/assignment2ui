﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class proximity_glow : MonoBehaviour {

    public GameObject player;//Reference object to player
    public float enableRange;//Range to start glow effect
    float distance; // distance between player and sprite
    bool m_animationEnabled = false;//enable or disable glow effect animation
    Vector2 m_spritePos; //store sprite position

    public bool CheckIsInRange(float range)
    {//return true if player is within range of sprite
        return (distance < range);
    }

    // Use this for initialization
    void Start () {
        m_spritePos = GetComponent<Transform>().position;//take sprite start position as it is a static object
        enableRange *= enableRange; //square the range
    }

    // Update is called once per frame
    void Update()
    {
        if (!player)
            player = Player.Instance.gameObject;
        Vector3 Playerpos = player.transform.position; //store current player position
        distance = new Vector2(Playerpos.x - m_spritePos.x, Playerpos.y - m_spritePos.y).sqrMagnitude; // get the distance squared of the player and the static sprite pos
        if (distance < enableRange)
        {//check if player is within range
            if (!m_animationEnabled)
            {//check if animation hasnt started
                m_animationEnabled = true; //start animation
                Debug.Log("Map glow animation Enabled");
            }
        }
        else if (distance > enableRange && m_animationEnabled)
        {// check if player out of range but animation hasnt ended yet
            if (GetComponent<SpriteRenderer>().color.a < 0.01f)
            {//check if the sprite alpha is almost invisible
                m_animationEnabled = false; // disable the animation
                Debug.Log("Map Glow animation Disabled");
            }          
        }
        else
        {//if animation is disabled
            if(GetComponent<Animator>() != null)
                GetComponent<Animator>().Play(0, 0, 0.5f); //continuousely play this part of the animation frame
        }
    }
}
