﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Saving : MonoBehaviour {

    // Use this for initialization
    public static Saving Instance;

    Stats playerStats;
    Health playerHealth;

    public List<int> weaponIndexesSave = new List<int>();
    public List<int> itemNormalIndexSave = new List<int>();

    bool saved = false;
    int hourSaved;
    int minsSaved;
    int killCount;
    bool vibration = true;

    //player info
	void Awake () {

        if (Instance == null)
        {
            Instance = this; //intialise this immediately
            DontDestroyOnLoad(gameObject);
            hourSaved = 0;
            minsSaved = 0;
            killCount = 0;
        }
        else if (Instance != this)
        {
            Destroy(this);
        }
    }
	
    public bool GetSaved
    {
        get { return saved; }
        set  { saved = value; }
    }

    public int GetKillCount
    {
        get { return killCount; }
        set { killCount = value; }
    }

    public int GetHourSaved
    {
        get { return hourSaved; }
        set { hourSaved = value; }
    }

    public int GetMinsSaved
    {
        get { return minsSaved; }
        set { minsSaved = value; }
    }

    public bool GetVibration
    {
        get { return vibration; }
        set { vibration = value; }
    }
}
