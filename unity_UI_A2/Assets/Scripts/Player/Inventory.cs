﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour {

    // Use this for initialization
    public static Inventory Instance;

    public List<ObjectType> weaponsList = new List<ObjectType>();
    public List<ObjectType> itemNormalList = new List<ObjectType>();

    public List<int> weaponIndexesSave = new List<int>();
    public List<int> itemNormalIndexSave = new List<int>();

    public int weaponSpace;
    public int itemNormalSpace; //for normal item

    public delegate void OnItemChanged();
    public OnItemChanged onItemChangedCallback;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }
        else if (Instance != this)
        {
            Destroy(this);

            //reinitialise
            for (int i = 0; i < Instance.weaponIndexesSave.Count; ++i)
            {
                if (Instance.weaponIndexesSave[i] == (int)Object_enum.ObjectType.AXE)
                {
                    AxeType Axe = Resources.Load<GameObject>("Objects/Prefabs/Axe class").GetComponent<AxeType>();
                    Axe.Init();
                    Instance.weaponsList[i] = Axe;
                }
                else if (Instance.weaponIndexesSave[i] == (int)Object_enum.ObjectType.KNIFE)
                {
                    KnifeType knife = Resources.Load<GameObject>("Objects/Prefabs/Knife Class").GetComponent<KnifeType>();
                    knife.Init();
                    Instance.weaponsList[i] = knife;
                }
                else if (Instance.weaponIndexesSave[i] == (int)Object_enum.ObjectType.PIPE)
                {
                    PipeType pipe = Resources.Load<GameObject>("Objects/Prefabs/Pipe class").GetComponent<PipeType>();
                    pipe.Init();
                    Instance.weaponsList[i] = pipe;
                }
            }

            for (int i = 0; i < Instance.itemNormalIndexSave.Count; ++i)
            {
                if (Instance.itemNormalIndexSave[i] == (int)Object_enum.ObjectType.HEALTHPACK)
                {
                    medicType med = Resources.Load<GameObject>("Objects/Prefabs/Medicclass").GetComponent<medicType>();
                    med.Init();
                    Instance.itemNormalList[i] = med;
                }
                else if (Instance.itemNormalIndexSave[i] == (int)Object_enum.ObjectType.PRESENT)
                {
                    PresentType present = Resources.Load<GameObject>("Objects/Prefabs/Present class").GetComponent<PresentType>();
                    present.Init();
                    Instance.itemNormalList[i] = present;
                }
            }
        }
    }

    public void AddItem(ObjectType obj)
    {
        //check if its weapon type and if it got space
        if (obj.clasifyObjType == ObjectType.ObjectClassify.WEAPON_OBJ && weaponsList.Count < weaponSpace)
        {
            weaponsList.Add(obj);
            weaponIndexesSave.Add((int)obj.GetObjectType());
        }

        //check if its item type and if got space
        if (obj.clasifyObjType == ObjectType.ObjectClassify.NORMAL_OBJ && itemNormalList.Count < itemNormalSpace)
        {
            itemNormalList.Add(obj);
            itemNormalIndexSave.Add((int)obj.GetObjectType());
        }

        if (onItemChangedCallback != null)
            onItemChangedCallback.Invoke();
    }

    public void Remove(ObjectType obj)
    {
        if (obj.clasifyObjType == ObjectType.ObjectClassify.WEAPON_OBJ)
        {
            weaponsList.Remove(obj);
            weaponIndexesSave.Remove((int)obj.GetObjectType());
        }

        if (obj.clasifyObjType == ObjectType.ObjectClassify.NORMAL_OBJ)
        {
            itemNormalList.Remove(obj);
            itemNormalIndexSave.Remove((int)obj.GetObjectType());
        }

        if (onItemChangedCallback != null)
            onItemChangedCallback.Invoke();
    }
}
