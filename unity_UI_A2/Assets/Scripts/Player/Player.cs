﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    // Use this for initialization
    public static Player Instance;

    private Stats playerStats;
    private Health playerHealth;

    public int maxStatPt;
    public int maxHealth;

    int followers;
    int ranking;
    int likes;

    int time;
    int kills;

    int totalHourPlayed;
    int totalMinsPlayed;

    float timer = 0.0f;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this; //intialise this immediately
            DontDestroyOnLoad(gameObject);

        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }

        playerStats = gameObject.GetComponent<Stats>();
        playerHealth = gameObject.GetComponent<Health>();
        playerHealth.GetTotalHealth = maxHealth;
        playerHealth.GetCurrHealth = maxHealth;
    }

    public void Update()
    {
        timer += Time.deltaTime;

        if (timer > 4.0f) //every 10s, will increase, in-game world version of time
        {
            ++totalMinsPlayed;
            ++time;
            timer = 0.0f;
        }

        if (time > 11) //time resets after 11pm
            time = 1;

        if (totalMinsPlayed > 59)
        {
            ++totalHourPlayed;
            totalMinsPlayed = 0;
        }
    }

    public Stats GetPlayerStat
    {
        get { return playerStats; }
    }

    public Health GetPlayerHealth
    {
        get { return playerHealth; }
    }

    public int GetPlayerFollowers
    {
        get { return followers; }
        set { followers = value; }
    }

    public int GetPlayerRanking
    {
        get { return ranking; }
        set { ranking = value; }
    }

    public int GetPlayerLikes
    {
        get { return likes; }
        set { likes = value; }
    }

    public int GetTime
    {
        get { return time; }
        set { time = value; }
    }

    public int GetKills
    {
        get { return kills; }
        set { kills = value; }
    }

    public int GetHour
    {
        get { return totalHourPlayed; }
        set { totalHourPlayed = value; }
    }

    public int GetMins
    {
        get { return totalMinsPlayed; }
        set { totalMinsPlayed = value; }
    }

    public void ResetPlayerInfo()
    {
        followers = 85;
        ranking = 100;
        likes = 97;
        kills = 0;
        playerHealth.GetCurrHealth = maxHealth;
        time = totalMinsPlayed = totalHourPlayed = 0;
    }
}
