﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats : MonoBehaviour {

    private void Awake()
    {
    }

    private int upgradePts;
    private int[] stats;
    private string[] statsDesciption;

    public enum STATS
    {
        ATTACK = 0,
        SPEECH,
        SNEAK,
        SELF_DEFENSE,
        TOTAL,
    };

    // Use this for initialization
    void Start () {
        stats = new int[(int)STATS.TOTAL] { 0,0,0, 0}; //the stats
        statsDesciption = new string[(int)STATS.TOTAL];
        InitialiseDesciptions();
        upgradePts = 10; //give it an example of 10 upgrade pts first to start
    }

    void InitialiseDesciptions()
    {
        statsDesciption[(int)STATS.ATTACK] = "Increases damage dealt when attacking others";
        statsDesciption[(int)STATS.SPEECH] = "Sways the student population easily to convince them on whatever you want";
        statsDesciption[(int)STATS.SNEAK] = "People will only notice you at a shorter distance. \n Allows for easier spying, evesdropping or stealing.";
        statsDesciption[(int)STATS.SELF_DEFENSE] = "Take lesser damage.";
    }

    public void SetUpdgradePts(int newPts)
    {
        upgradePts = newPts;
    }

    public int GetUpdgradePts
    { get{ return upgradePts; }
        set { upgradePts = value; }
    }

    public int[] Getstats
    { get { return stats; }
        set { stats = value; }
    }

    public string[] GetstatDescriptions
    {
        get { return statsDesciption; }
    }
}
