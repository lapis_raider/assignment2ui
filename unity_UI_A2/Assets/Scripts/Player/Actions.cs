﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class Actions : MonoBehaviour {

    // Use this for initialization
    public TextMeshProUGUI text;

    public void Threaten()
    {
        Player.Instance.GetPlayerFollowers = Player.Instance.GetPlayerFollowers - 55;
        Player.Instance.GetPlayerLikes = Player.Instance.GetPlayerLikes - 55;
        Player.Instance.GetPlayerRanking = Player.Instance.GetPlayerLikes - 55;
        text.SetText("Meanie");
    }

    public void Compliment()
    {
        Player.Instance.GetPlayerFollowers = Player.Instance.GetPlayerFollowers + 55;
        Player.Instance.GetPlayerLikes = Player.Instance.GetPlayerLikes + 55;
        Player.Instance.GetPlayerRanking = Player.Instance.GetPlayerLikes + 55;
        text.SetText("Thanks");
    }

    public void Nothing()
    {
        text.SetText("Bye");
    }

    public void Hello()
    {
        text.SetText("Yes?");
    }
}
