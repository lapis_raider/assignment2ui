﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {

	// Use this for initialization
    private int currentHealth;
    private int totalHealth;

	void Awake () {
    }

    public int GetCurrHealth
    {
        get { return currentHealth; }
        set { currentHealth = value; }
    }

    public int GetTotalHealth
    {
        get { return totalHealth; }
        set { totalHealth = value; }
    }
}
