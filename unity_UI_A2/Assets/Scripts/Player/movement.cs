﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movement : MonoBehaviour {

    // Use this for initialization
    public float m_speed; // speed variable
    public float m_constraintSpeed; // max and minimum speed
    Vector2 m_dPadOriginPos; // origin of the dpad
    bool dPadOn = false; // flag if dpad is being used
    bool movementEnabled = true; //  flag to enabled movement

    public void movementFlagUpdate()
    {//function to change the bool to the oppositer of current bool
        movementEnabled = !movementEnabled;
    }
    public void movementFlagUpdate(bool boo_)
    {//function to change the bool to the oppositer of current bool
        movementEnabled = boo_;
    }
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        //check if player is able to move
        if (!movementEnabled)
            return;

        //check for touches
        foreach (Touch touch in Input.touches)
        {
            
            if (touch.fingerId == 0)
            {//check if there is a first touch
                if (Input.GetTouch(0).phase == TouchPhase.Began)
                {//check if Touch has just entered
                    Debug.Log("First finger entered!");
                    //set DPad origin
                    m_dPadOriginPos = touch.position;
                    //set Dpad to being used
                    dPadOn = true;
                }
                else if (Input.GetTouch(0).phase == TouchPhase.Ended)
                {//chekc if touch has left
                    Debug.Log("First finger left.");
                    //set Dpad is not being used
                    dPadOn = false;
                }
            }
        }
        if (Input.touches.Length < 1)
            dPadOn = false;
            if (dPadOn)
        { //if the invisible Dpad is being used
          //get displacement from the origin of the dpad and the current touch position
          

            Vector2 displacement = -(m_dPadOriginPos - Input.GetTouch(0).position) * m_speed;
            //clamp the displacement
            displacement = new Vector2(Mathf.Clamp(displacement.x, -m_constraintSpeed, m_constraintSpeed), Mathf.Clamp(displacement.y, -m_constraintSpeed, m_constraintSpeed));
            //Update position
                GetComponent<Rigidbody2D>().position += displacement;
        }

        //TEMPERORY WASD MOVEMENT
        float xAxis = Input.GetAxis("Horizontal"); //movement X axis
        float yAxis = Input.GetAxis("Vertical"); // movement Y axis
        Vector2 move = new Vector2(
                                   Mathf.Clamp(xAxis, -m_constraintSpeed, m_constraintSpeed),
                                   Mathf.Clamp(yAxis, -m_constraintSpeed, m_constraintSpeed)
                                  );
        GetComponent<Rigidbody2D>().position += move;

    }
}
