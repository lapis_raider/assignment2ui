﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnifeType : ObjectType
{ 
    // Use this for initialization
    void Start()
    {
        m_Type = Object_enum.ObjectType.KNIFE;
        clasifyObjType = ObjectType.ObjectClassify.WEAPON_OBJ;
        objName = "Knife";
        objDescription = "Stabby Stabby. Induces bleeding";
        objSprite = Resources.Load<Sprite>("Objects/knife");
    }

    public virtual void Init()
    {
        m_Type = Object_enum.ObjectType.KNIFE;
        clasifyObjType = ObjectType.ObjectClassify.WEAPON_OBJ;
        objName = "Knife";
        objDescription = "Stabby Stabby. Induces bleeding";
        objSprite = Resources.Load<Sprite>("Objects/knife");
    }
}
