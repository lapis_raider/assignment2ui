﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class medicType : ObjectType
{
    // Use this for initialization
    void Awake()
    {
        m_Type = Object_enum.ObjectType.HEALTHPACK;
        clasifyObjType = ObjectType.ObjectClassify.NORMAL_OBJ;
        objName = "Health pack";
        objDescription = "Recovers some health";
        objSprite = Resources.Load<Sprite>("Objects/medic");
    }

    public virtual void Init()
    {
        m_Type = Object_enum.ObjectType.HEALTHPACK;
        clasifyObjType = ObjectType.ObjectClassify.NORMAL_OBJ;
        objName = "Health pack";
        objDescription = "Recovers some health";
        objSprite = Resources.Load<Sprite>("Objects/medic");
    }
}

