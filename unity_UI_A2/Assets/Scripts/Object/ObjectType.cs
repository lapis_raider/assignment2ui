﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectType : MonoBehaviour
{
    public Object_enum.ObjectType m_Type;

    public enum ObjectClassify
    {
        WEAPON_OBJ,
        NORMAL_OBJ
    };

    public ObjectClassify clasifyObjType;
    public string objName;
    public string objDescription;
    public Sprite objSprite;

    // Use this for initialization

    // Update is called once per frame
    void SetType(Object_enum.ObjectType type)
    {
        m_Type = type;
    }
    public Object_enum.ObjectType GetObjectType()
    {
        return m_Type;
    }

    public virtual void Init()
    {
    }
}
