﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeType : ObjectType
{
    // Use this for initialization
    void Awake()
    {
        m_Type = Object_enum.ObjectType.PIPE;
        clasifyObjType = ObjectType.ObjectClassify.WEAPON_OBJ;
        objName = "Pipe";
        objDescription = "Hit someone, increases attack";
        objSprite = Resources.Load<Sprite>("Objects/pipe");
    }

    public virtual void Init()
    {
        m_Type = Object_enum.ObjectType.PIPE;
        clasifyObjType = ObjectType.ObjectClassify.WEAPON_OBJ;
        objName = "Pipe";
        objDescription = "Hit someone, increases attack";
        objSprite = Resources.Load<Sprite>("Objects/pipe");
    }
}
