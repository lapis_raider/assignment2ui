﻿using UnityEngine;

public class Object_enum : MonoBehaviour {

    public enum ObjectType
    {
        MAP,
        AXE,
        KNIFE,
        PIPE,
        HEALTHPACK,
        PRESENT,
    };
}
