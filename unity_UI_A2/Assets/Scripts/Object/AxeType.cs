﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AxeType : ObjectType {

	// Use this for initialization
	void Awake () {
       m_Type = Object_enum.ObjectType.AXE;
       clasifyObjType = ObjectType.ObjectClassify.WEAPON_OBJ;
       objName = "Axe";
       objDescription = "Chop enemies up, does more damage";
       objSprite = Resources.Load<Sprite>("Objects/axe");
    }

    public virtual void Init()
    {
        m_Type = Object_enum.ObjectType.AXE;
        clasifyObjType = ObjectType.ObjectClassify.WEAPON_OBJ;
        objName = "Axe";
        objDescription = "Chop enemies up, does more damage";
        objSprite = Resources.Load<Sprite>("Objects/axe");
    }
}
