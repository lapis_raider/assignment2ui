﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PresentType : ObjectType
{
    // Use this for initialization
    void Awake()
    {
        m_Type = Object_enum.ObjectType.PRESENT;
        clasifyObjType = ObjectType.ObjectClassify.NORMAL_OBJ;
        objName = "Present";
        objDescription = "Give to someone, to increase popularity";
        objSprite = Resources.Load<Sprite>("Objects/Present_sprite");
    }

    public virtual void Init()
    {
        m_Type = Object_enum.ObjectType.PRESENT;
        clasifyObjType = ObjectType.ObjectClassify.NORMAL_OBJ;
        objName = "Present";
        objDescription = "Give to someone, to increase popularity";
        objSprite = Resources.Load<Sprite>("Objects/Present_sprite");
    }
}
