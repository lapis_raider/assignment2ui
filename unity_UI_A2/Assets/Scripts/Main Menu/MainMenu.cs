﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    public GameObject newGamePopUp;

    public void StartGame() //use to load next level
    {
        SceneManager.LoadScene("Game");
    }

    public void OnNewGame() //use to load next level
    {
        if (!Saving.Instance.GetSaved)
            SceneManager.LoadScene("Game");
        else
        {
            newGamePopUp.SetActive(true);
            newGamePopUp.GetComponent<NewGamePopUp>().Init();
            gameObject.SetActive(false);
        }
    }
}
