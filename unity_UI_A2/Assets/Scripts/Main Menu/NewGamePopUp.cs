﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class NewGamePopUp : MonoBehaviour
{
    public GameObject mainMenu;
    public TextMeshProUGUI time;
    public TextMeshProUGUI killsCount;

    public void Init()
    {
        time.SetText("Time: " + Saving.Instance.GetHourSaved.ToString() + ":" + Saving.Instance.GetMinsSaved.ToString());
        killsCount.SetText("Kills: " + Saving.Instance.GetKillCount.ToString());
    }


    public void OnYesClick()
    {
        Saving.Instance.GetSaved = false; //make it 'delete' its data

        Saving.Instance.GetSaved = false;
        Saving.Instance.GetKillCount = 0;
        Saving.Instance.GetHourSaved = Saving.Instance.GetMinsSaved = 0;

        SceneManager.LoadScene("Game"); //load the game
    }

    public void OnNoClick() //go back to main menu
    {
        gameObject.SetActive(false);
        mainMenu.SetActive(true);
    }
}
