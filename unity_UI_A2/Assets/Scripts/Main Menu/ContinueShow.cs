﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContinueShow : MonoBehaviour {

    // Use this for initialization
    void Start() {
        if (Saving.Instance.GetSaved)
            gameObject.SetActive(true);
        else
            gameObject.SetActive(false);
    }
}
